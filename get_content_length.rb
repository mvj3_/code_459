#这段代码应该是可以很好的解决获取指定URL的内容大小的问题了

require 'open-uri'
require 'net/http'
require 'date'
require 'zlib' 


asset  = "http://code.eoe.cn"
headers = { "accept-encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3" }
url = URI.parse asset
puts url
res = Net::HTTP.start(url.host, url.port) {|http|
  http.get(asset, headers)
}

headers = res.to_hash

#logger
puts " ---log headers start ---"
headers.each do |k,v| 
  puts "#{k} = #{v}"
end 
puts " ---log headers end ---"

gzipped = headers['content-encoding'] && headers['content-encoding'][0] == "gzip"

content = gzipped ? Zlib::GzipReader.new(StringIO.new(res.body)).read : res.body 

full_length = content.length

compressed_length = (headers["content-length"] && headers["content-length"][0] || res.body.length)

puts " \r\n---output ---"
puts "full_length=#{full_length} ; compressed_length=#{compressed_length}"


=begin
http://code.eoe.cn
 ---log headers start ---
content-type = text/html; charset=utf-8
x-request-id = 3bb0fa03b9126a937da7f44fbc6ddbf1
x-ua-compatible = IE=Edge,chrome=1
date = Sat, 09 Mar 2013 12:38:04 GMT
cache-control = max-age=0, private, must-revalidate
x-rack-cache = miss
status = 200 OK
transfer-encoding = chunked
server = nginx/1.0.6
etag = "a89b1757b4d28912812081c52719ded2"
set-cookie = uhash=0bdf35a3c1fd79c755db3a4a26a5d3d6; path=/code.eoe.cn=BAh7B0kiD3Nlc3Npb25faWQGOgZFRkkiJTJlMzZlYzUxYzMwNjMyZWVmNjE0MzBkNWY0MTBhNzYwBjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMVFVbFZORDlwWjJUZlZpYllBZWx1cGIxd01SNUY5YVBmb3UwVHFScTdJQ2c9BjsARg%3D%3D--e875d0cf2807b88e32cdca75ca7c973fa5176f3f; path=/; expires=Tue, 09-Apr-2013 12:38:04 GMT; HttpOnly
x-runtime = 0.203870
content-encoding = gzip
connection = keep-alive
 ---log headers end ---
  
---output ---
full_length=27026 ; compressed_length=7293
=end